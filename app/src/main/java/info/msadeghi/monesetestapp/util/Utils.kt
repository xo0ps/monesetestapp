package info.msadeghi.monesetestapp.util

import android.content.Context
import android.util.TypedValue
import android.widget.ImageView
import android.widget.Toast
import com.squareup.picasso.Picasso
import info.msadeghi.monesetestapp.R
import kotlinx.android.synthetic.main.fragment_rocketdetail.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by mahdi on 10/18/16.
 */

object Utils {

    fun log(str: String?) {

        if (str == null || str.length == 0)
            return

        val maxLength = 4000
        val split = str.length / maxLength + 1
        val mod = str.length % maxLength
        for (i in 0 until split) {
            try {
                Timber.w(str.substring(i * maxLength, (i + 1) * maxLength))
            } catch (e: Exception) {
                Timber.w(str.substring(i * maxLength, i * maxLength + mod))
            }

        }
    }

    fun log(e: Throwable) {
        Timber.e(e)
    }

    fun showError(context: Context?, msg: String?) {

        if (context == null || msg == null) return
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    fun getDate(date: Long): String {

        val formatter = SimpleDateFormat.getDateTimeInstance()
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date * 1000
        return formatter.format(calendar.time)
    }

    fun loadImage(src: String, target: ImageView) {

        Picasso.get().load(src).error(R.mipmap.ic_launcher).placeholder(R.mipmap.ic_launcher).into(target)

    }
}

