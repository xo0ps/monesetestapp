package info.msadeghi.monesetestapp.data.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class Rocket(

    @SerializedName("rocket_id")
    val rocketId: String,
    @SerializedName("rocket_name")
    val rocketName: String,
    val description: String,
    val country: String,
    val company: String,
    @SerializedName("flickr_images")
    val flickrImages: Array<String>
)