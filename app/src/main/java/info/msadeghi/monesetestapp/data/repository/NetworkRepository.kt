package info.msadeghi.monesetestapp.data.repository

import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.data.model.response.Rocket
import info.msadeghi.monesetestapp.data.network.launch.LaunchApiService
import info.msadeghi.monesetestapp.data.network.rocket.RocketApiService
import io.reactivex.Single
import javax.inject.Inject


/**
 * Created by mahdi on 7/18/16.
 */
class NetworkRepository
@Inject constructor(val rocketApi: RocketApiService,
                    val launchApi: LaunchApiService) {

    fun getAllLaunches(): Single<ArrayList<Launch>> = launchApi.getAllLaunches()

    fun getRocketDetail(id: String): Single<Rocket> = rocketApi.getRocketDetail(id)


}
