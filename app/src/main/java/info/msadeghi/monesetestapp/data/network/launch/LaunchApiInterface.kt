package info.msadeghi.monesetestapp.data.network.launch

import info.msadeghi.monesetestapp.data.model.response.Launch
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by mahdi on 24/05/2016.
 */
interface LaunchApiInterface {

    @GET("launches")
    fun getAllLaunches(): Single<ArrayList<Launch>>

}
