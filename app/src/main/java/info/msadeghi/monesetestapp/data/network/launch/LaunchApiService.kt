package info.msadeghi.monesetestapp.data.network.launch

import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.data.network.rocket.RocketApiInterface
import io.reactivex.Single


/**
 * Created by mahdi on 7/18/16.
 */
class LaunchApiService(private val launchApi: LaunchApiInterface) {

    fun getAllLaunches(): Single<ArrayList<Launch>> = launchApi.getAllLaunches()

}
