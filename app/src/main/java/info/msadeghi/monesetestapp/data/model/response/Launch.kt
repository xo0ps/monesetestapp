package info.msadeghi.monesetestapp.data.model.response

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class Launch(

    @SerializedName("flight_number")
    val flightNumber: Int,
    @SerializedName("mission_name")
    val missionName: String,
    @SerializedName("launch_date_unix")
    var launchDateUnix: Long,
    val tbd: Boolean,
    val rocket: Rocket
)