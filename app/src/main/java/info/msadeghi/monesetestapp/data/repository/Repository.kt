package info.msadeghi.monesetestapp.data.repository

import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.data.model.response.Rocket
import info.msadeghi.monesetestapp.data.network.launch.LaunchApiService
import info.msadeghi.monesetestapp.data.network.rocket.RocketApiService
import io.reactivex.Single
import javax.inject.Inject


/**
 * Created by mahdi on 7/18/16.
 */
class Repository
@Inject constructor(val networkRepository: NetworkRepository,
                    val sharedPreferenceRepository: SharedPreferenceRepository) {

    fun getAllLaunches(): Single<ArrayList<Launch>> = networkRepository.getAllLaunches()

    fun getRocketDetail(id: String): Single<Rocket> = networkRepository.getRocketDetail(id)

    //getting other data from other repositories goes below
    //like sharedpreferences, room db, files, ...

    fun getToken() = sharedPreferenceRepository.getToken()
}
