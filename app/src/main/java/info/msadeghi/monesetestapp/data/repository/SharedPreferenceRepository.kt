package info.msadeghi.monesetestapp.data.repository

import android.content.SharedPreferences
import info.msadeghi.monesetestapp.common.Constants
import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.data.model.response.Rocket
import info.msadeghi.monesetestapp.data.network.launch.LaunchApiService
import info.msadeghi.monesetestapp.data.network.rocket.RocketApiService
import io.reactivex.Single
import javax.inject.Inject


/**
 * Created by mahdi on 7/18/16.
 */
class SharedPreferenceRepository
@Inject constructor(val sharedPreferences: SharedPreferences) {

    //sample for interaction with sharedpreferences
    fun getToken(): String? = sharedPreferences.getString("TOKEN", "")
}
