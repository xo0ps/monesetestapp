package info.msadeghi.monesetestapp.data.network.rocket

import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.data.model.response.Rocket
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by mahdi on 24/05/2016.
 */
interface RocketApiInterface {

    @GET("rockets/{id}")
    fun getRocketDetail(@Path("id") id: String): Single<Rocket>

}
