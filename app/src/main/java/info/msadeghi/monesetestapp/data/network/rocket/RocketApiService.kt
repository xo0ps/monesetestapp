package info.msadeghi.monesetestapp.data.network.rocket

import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.data.model.response.Rocket
import info.msadeghi.monesetestapp.data.network.rocket.RocketApiInterface
import io.reactivex.Single


/**
 * Created by mahdi on 7/18/16.
 */
class RocketApiService(private val rocketApi: RocketApiInterface) {

    fun getRocketDetail(id: String): Single<Rocket> = rocketApi.getRocketDetail(id)

}
