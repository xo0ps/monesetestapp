package info.msadeghi.monesetestapp.ui.launchlist

import android.app.Activity
import android.graphics.drawable.ColorDrawable
import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.ui.adapter.LaunchAdapter
import info.msadeghi.monesetestapp.ui.rocketdetail.RocketDetailFragment
import info.msadeghi.monesetestapp.util.Utils
import info.msadeghi.revoluttestapp.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_launchlist.*
import javax.inject.Inject
import android.view.*
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import info.msadeghi.monesetestapp.R


class LaunchListFragment : BaseFragment() {

    @Inject
    lateinit var vm: LaunchListViewModel

    lateinit var launchAdapter: LaunchAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_launchlist, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipe.setOnRefreshListener {
            vm.refresh()
        }

        launchList.apply {
            val linearLayoutManager = LinearLayoutManager(activity)
            layoutManager = linearLayoutManager

            val divider = DividerItemDecoration(context, linearLayoutManager.orientation)
            divider.setDrawable(ColorDrawable(ContextCompat.getColor(context, android.R.color.white)))
            addItemDecoration(divider)

            launchAdapter = LaunchAdapter(object : LaunchAdapter.OnItemActionListener {
                override fun onClick(item: Launch) {
                    navigate(RocketDetailFragment.getRocketDetailFragment(item.rocket.rocketId))
                }
            })
            adapter = launchAdapter
        }

        vm.launches.observe(viewLifecycleOwner, Observer {
            launchAdapter.submitList(it)
        })

        vm.error.observe(viewLifecycleOwner, Observer {
            Utils.showError(activity as Activity, it)
        })

        vm.loading.observe(viewLifecycleOwner, Observer {
            swipe.isRefreshing = it
        })

        vm.getLaunches()
        setToolbarSubtitle(getString(R.string.title_launchlist))

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.launch_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.filter -> {
                item.setChecked(!item.isChecked)
                vm.switchTBD()
                return true
            }
        }
        return false
    }
}