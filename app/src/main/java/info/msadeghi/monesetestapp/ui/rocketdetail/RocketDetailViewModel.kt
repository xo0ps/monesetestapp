package info.msadeghi.monesetestapp.ui.rocketdetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import info.msadeghi.monesetestapp.data.model.response.Rocket
import info.msadeghi.monesetestapp.data.repository.NetworkRepository
import info.msadeghi.monesetestapp.data.repository.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RocketDetailViewModel
@Inject constructor(val repository: Repository): ViewModel() {

    private val disposable by lazy {
        CompositeDisposable()
    }

    val rocket = MutableLiveData<Rocket>()
    val error = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()

    fun getRocketDetail(id: String) {

        loading.postValue(true)
        disposable.add(repository.getRocketDetail(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<Rocket>() {
                override fun onSuccess(resp: Rocket) {

                    loading.postValue(false)
                    rocket.postValue(resp)

                }

                override fun onError(e: Throwable) {
                    loading.postValue(false)
                    error.value = e.message
                }
            }))
    }

}