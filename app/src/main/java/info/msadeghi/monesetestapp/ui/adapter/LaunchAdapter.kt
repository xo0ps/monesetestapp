package info.msadeghi.monesetestapp.ui.adapter

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import info.msadeghi.monesetestapp.R
import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.data.model.response.Rocket
import info.msadeghi.monesetestapp.util.Utils
import kotlinx.android.synthetic.main.row_launch.view.*
import java.util.*


class LaunchAdapter(private var onItemActionListener: OnItemActionListener?) : ListAdapter<Launch, LaunchAdapter.ViewHolder>(LaunchDiffUtilCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_launch, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(launch: Launch){

            containerView.number.text = launch.flightNumber.toString()
            containerView.name.text = launch.missionName
            containerView.date.text = Utils.getDate(launch.launchDateUnix)
            containerView.setOnClickListener {
                onItemActionListener?.onClick(launch)
            }
        }

    }

    class LaunchDiffUtilCallBack : DiffUtil.ItemCallback<Launch>() {

        override fun areItemsTheSame(oldItem: Launch, newItem: Launch): Boolean {
            return oldItem.flightNumber == newItem.flightNumber
        }

        override fun areContentsTheSame(oldItem: Launch, newItem: Launch): Boolean {
            return oldItem == newItem
        }

    }

    interface OnItemActionListener {
        fun onClick(item: Launch)
    }
}
