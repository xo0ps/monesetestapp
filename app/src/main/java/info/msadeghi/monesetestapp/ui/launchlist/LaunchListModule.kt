package info.msadeghi.monesetestapp.ui.launchlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import info.msadeghi.monesetestapp.data.repository.NetworkRepository
import info.msadeghi.monesetestapp.data.repository.Repository
import info.msadeghi.monesetestapp.di.ViewModelKey

@Module(includes = [
    LaunchListModule.ProvideViewModel::class
])
abstract class LaunchListModule {

    @ContributesAndroidInjector(modules = [
        InjectViewModel::class
    ])
    abstract fun bind(): LaunchListFragment

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(LaunchListViewModel::class)
        fun provideLaunchListViewModel(repository: Repository): ViewModel =
            LaunchListViewModel(repository)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideLaunchListViewModel(factory: ViewModelProvider.Factory, target: LaunchListFragment): LaunchListViewModel =
                ViewModelProviders.of(target, factory).get(LaunchListViewModel::class.java)
    }

}