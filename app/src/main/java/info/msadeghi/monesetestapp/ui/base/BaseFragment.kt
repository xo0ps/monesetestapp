package info.msadeghi.revoluttestapp.ui.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import dagger.android.support.AndroidSupportInjection
import info.msadeghi.monesetestapp.R
import info.msadeghi.monesetestapp.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_main.*

abstract class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    fun setToolbarSubtitle(title: String) {
        (activity as MainActivity).setToolbarSubtitle(title)
    }

    fun navigate(fragment: Fragment?) {
        (activity as MainActivity).navigate(fragment)
    }
}