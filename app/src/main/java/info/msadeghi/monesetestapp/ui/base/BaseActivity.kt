package info.msadeghi.revoluttestapp.ui.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import java.util.*

abstract class BaseActivity : AppCompatActivity() {

    abstract fun setToolbarSubtitle(title: String)
}