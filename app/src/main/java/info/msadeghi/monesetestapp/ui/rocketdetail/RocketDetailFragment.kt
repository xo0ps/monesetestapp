package info.msadeghi.monesetestapp.ui.rocketdetail

import android.app.Activity
import android.graphics.drawable.ColorDrawable
import androidx.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import info.msadeghi.monesetestapp.R
import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.ui.adapter.LaunchAdapter
import info.msadeghi.monesetestapp.util.Utils
import info.msadeghi.revoluttestapp.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_launchlist.*
import kotlinx.android.synthetic.main.fragment_launchlist.swipe
import kotlinx.android.synthetic.main.fragment_rocketdetail.*
import javax.inject.Inject


class RocketDetailFragment : BaseFragment() {

    @Inject
    lateinit var vm: RocketDetailViewModel

    companion object {
        val ROCKET_ID = "ROCKET_ID"

        fun getRocketDetailFragment(id: String): RocketDetailFragment {
            return RocketDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(ROCKET_ID, id)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_rocketdetail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = arguments?.getString(ROCKET_ID) ?: return

        swipe.setOnRefreshListener {
            getRocketDetail(id)
        }

        vm.rocket.observe(viewLifecycleOwner, Observer {

            name.text = it.rocketName
            country.text = it.country
            company.text = it.company
            description.text = it.description

            if (it.flickrImages.size > 0)
                Utils.loadImage(it.flickrImages[0], image)
        })

        vm.error.observe(viewLifecycleOwner, Observer {
            Utils.showError(activity as Activity, it)
        })

        vm.loading.observe(viewLifecycleOwner, Observer {
            swipe.isRefreshing = it
        })

        getRocketDetail(id)

        setToolbarSubtitle(getString(R.string.title_rocketdetail))
    }

    private fun getRocketDetail(id: String) = vm.getRocketDetail(id)

}