package info.msadeghi.monesetestapp.ui.rocketdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import info.msadeghi.monesetestapp.data.repository.NetworkRepository
import info.msadeghi.monesetestapp.data.repository.Repository
import info.msadeghi.monesetestapp.di.ViewModelKey

@Module(includes = [
    RocketDetailModule.ProvideViewModel::class
])
abstract class RocketDetailModule {

    @ContributesAndroidInjector(modules = [
        InjectViewModel::class
    ])
    abstract fun bind(): RocketDetailFragment

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(RocketDetailViewModel::class)
        fun provideRocketDetailViewModel(repository: Repository): ViewModel =
            RocketDetailViewModel(repository)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideRocketDetailViewModel(factory: ViewModelProvider.Factory, target: RocketDetailFragment): RocketDetailViewModel =
                ViewModelProviders.of(target, factory).get(RocketDetailViewModel::class.java)
    }

}