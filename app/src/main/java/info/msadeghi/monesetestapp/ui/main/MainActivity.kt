package info.msadeghi.monesetestapp.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import info.msadeghi.monesetestapp.R
import info.msadeghi.monesetestapp.ui.launchlist.LaunchListFragment
import info.msadeghi.revoluttestapp.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.fragment.app.FragmentManager
import info.msadeghi.monesetestapp.util.Utils


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        addBackStackChangeListener()
        navigate(LaunchListFragment())
    }

    fun navigate(fragment: Fragment?) {

        if (fragment != null) {
            val current = supportFragmentManager.findFragmentByTag(fragment.javaClass.canonicalName)
            if (current != null && current.isVisible)
                return

            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter, R.anim.exit)
            fragmentTransaction.replace(
                R.id.fragment_host,
                fragment,
                fragment.javaClass.canonicalName
            )
            fragmentTransaction.addToBackStack(fragment.javaClass.canonicalName)
            fragmentTransaction.commitAllowingStateLoss()
        }

    }

    private fun addBackStackChangeListener() {
        supportFragmentManager.addOnBackStackChangedListener {
            when(supportFragmentManager.backStackEntryCount) {
                0 -> finish()
                1 -> {
                    supportActionBar?.setHomeAsUpIndicator(R.mipmap.ic_launcher)
                    toolbar.setNavigationOnClickListener { /* disable onclicklistener */ }
                }
                else -> {
                    supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
                    toolbar.setNavigationOnClickListener {
                        supportFragmentManager.popBackStack()
                    }
                }
            }
        }
    }

    override fun setToolbarSubtitle(title: String) {
        supportActionBar?.subtitle = title
    }
}
