package info.msadeghi.monesetestapp.ui.launchlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import info.msadeghi.monesetestapp.data.model.response.Launch
import info.msadeghi.monesetestapp.data.repository.NetworkRepository
import info.msadeghi.monesetestapp.data.repository.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LaunchListViewModel
@Inject constructor(val repository: Repository): ViewModel() {

    private val disposable by lazy {
        CompositeDisposable()
    }

    val launches = MutableLiveData<ArrayList<Launch>>()
    val error = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    private var isTBD = false
    private var allLaunches = ArrayList<Launch>()

    fun getLaunches() {

        if (allLaunches.size > 0)
        {
            load()
            return
        }

        refresh()

    }

    fun switchTBD() {
        isTBD = !isTBD
        load()
    }

    fun refresh() {

        loading.postValue(true)
        disposable.add(repository.getAllLaunches()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<ArrayList<Launch>>() {
                override fun onSuccess(resp: ArrayList<Launch>) {

                    loading.postValue(false)
                    allLaunches = resp
                    load()
                }

                override fun onError(e: Throwable) {
                    loading.postValue(false)
                    error.value = e.message
                }
            }))
    }

    private fun load() {

        if (!isTBD)
            launches.postValue(allLaunches)
        else
        {
            val newList = ArrayList<Launch>()
            allLaunches.forEach {
                if (it.tbd)
                    newList.add(it)
            }
            launches.postValue(newList)
        }
    }

}