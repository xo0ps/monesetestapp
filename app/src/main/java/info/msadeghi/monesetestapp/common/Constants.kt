package info.msadeghi.monesetestapp.common

import android.os.Build
import info.msadeghi.monesetestapp.BuildConfig

/**
 * Created by mahdi on 10/18/16.
 */

object Constants {

    val RELEASE = !BuildConfig.DEBUG

    object WS {
        val BASE = "https://api.spacexdata.com/v3/"
        val NETWORK_TIMEOUT_SECONDS = 15
    }
}
