package info.msadeghi.monesetestapp.common

import android.app.Application
import androidx.fragment.app.Fragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import info.msadeghi.monesetestapp.di.AppComponent
import info.msadeghi.monesetestapp.di.DaggerAppComponent
import timber.log.Timber
import javax.inject.Inject


class App : Application(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>
    override fun supportFragmentInjector() = fragmentInjector

    lateinit var component: AppComponent

    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent.builder()
            .application(this)
            .context(this)
            .build()
        component.inject(this)

        instance = this

        if (!Constants.RELEASE) {
            Timber.plant(Timber.DebugTree())
        }

    }

}