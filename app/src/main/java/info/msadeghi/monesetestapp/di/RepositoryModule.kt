package info.msadeghi.monesetestapp.di

import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import info.msadeghi.monesetestapp.common.App
import info.msadeghi.monesetestapp.common.Constants
import info.msadeghi.monesetestapp.data.repository.NetworkRepository
import info.msadeghi.monesetestapp.data.network.launch.LaunchApiInterface
import info.msadeghi.monesetestapp.data.network.launch.LaunchApiService
import info.msadeghi.monesetestapp.data.network.rocket.RocketApiInterface
import info.msadeghi.monesetestapp.data.network.rocket.RocketApiService
import info.msadeghi.monesetestapp.data.repository.SharedPreferenceRepository
import info.msadeghi.monesetestapp.util.Utils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import javax.inject.Singleton
import java.util.concurrent.TimeUnit

/**
 * Created by mahdi on 22/05/16.
 */
@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideSharedPreferencesRepository(sharedPreferences: SharedPreferences) =
        SharedPreferenceRepository(sharedPreferences)

    @Provides
    @Singleton
    fun provideSharedPreferences() = PreferenceManager.getDefaultSharedPreferences(App.instance)

}
