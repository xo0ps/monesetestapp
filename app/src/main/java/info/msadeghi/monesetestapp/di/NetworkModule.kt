package info.msadeghi.monesetestapp.di

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import info.msadeghi.monesetestapp.common.Constants
import info.msadeghi.monesetestapp.data.repository.NetworkRepository
import info.msadeghi.monesetestapp.data.network.launch.LaunchApiInterface
import info.msadeghi.monesetestapp.data.network.launch.LaunchApiService
import info.msadeghi.monesetestapp.data.network.rocket.RocketApiInterface
import info.msadeghi.monesetestapp.data.network.rocket.RocketApiService
import info.msadeghi.monesetestapp.util.Utils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import javax.inject.Singleton
import java.util.concurrent.TimeUnit

/**
 * Created by mahdi on 22/05/16.
 */
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor,
                            networkTimeoutInSeconds: Int,
                            isDebug: Boolean): OkHttpClient {

        val okHttpClient = OkHttpClient.Builder()
            .readTimeout(networkTimeoutInSeconds.toLong(), TimeUnit.SECONDS)
            .connectTimeout(networkTimeoutInSeconds.toLong(), TimeUnit.SECONDS)
            .writeTimeout(networkTimeoutInSeconds.toLong(), TimeUnit.SECONDS)
            .retryOnConnectionFailure(false)

        if (isDebug)
            okHttpClient.addInterceptor(loggingInterceptor)

        return okHttpClient.build()
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor { message -> Utils.log(message) }
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    @Provides
    @Singleton
    fun provideRocketApiService(retrofit: Retrofit) = RocketApiService(
        retrofit.create(RocketApiInterface::class.java))

    @Provides
    @Singleton
    fun provideLaunchApiService(retrofit: Retrofit) = LaunchApiService(
        retrofit.create(LaunchApiInterface::class.java))

    @Provides
    @Singleton
    fun provideNetworkRepository(rocketService: RocketApiService,
                                 launchService: LaunchApiService) =
        NetworkRepository(rocketService, launchService)

    @Provides
    @Singleton
    fun provideRetrofit(baseUrl: String,
                        converterFactory: Converter.Factory,
                        callAdapterFactory: CallAdapter.Factory,
                        okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(callAdapterFactory)
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideGson() = Gson()

    @Provides
    @Singleton
    fun provideCallAdapterFactory(): CallAdapter.Factory = RxJava2CallAdapterFactory.create()

    @Provides
    @Singleton
    fun provideJsonConverterFactory(gson: Gson): Converter.Factory = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    fun provideIsDebug() = !Constants.RELEASE

    @Provides
    @Singleton
    fun provideNetworkTimeoutInSeconds() = Constants.WS.NETWORK_TIMEOUT_SECONDS

    @Provides
    @Singleton
    fun provideBaseUrl() = Constants.WS.BASE

}
