package info.msadeghi.monesetestapp.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import info.msadeghi.monesetestapp.common.App
import javax.inject.Singleton

@Component(modules = [
    AndroidSupportInjectionModule::class,
    UIModule::class,
    NetworkModule::class,
    RepositoryModule::class
])
@Singleton
interface AppComponent {

    fun inject(app: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: App): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }
}