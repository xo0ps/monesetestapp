package info.msadeghi.monesetestapp.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import info.msadeghi.monesetestapp.ui.launchlist.LaunchListModule
import info.msadeghi.monesetestapp.ui.rocketdetail.RocketDetailModule

import javax.inject.Provider

/**
 * Created by mahdi on 22/05/16.
 */
@Module(includes = [
    LaunchListModule::class,
    RocketDetailModule::class
])
class UIModule {

    @Provides
    fun provideViewModelFactory(providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>):
            ViewModelProvider.Factory = AppViewModelFactory(providers)
}
